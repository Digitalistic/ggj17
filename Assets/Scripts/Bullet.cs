﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour 
{
    Rigidbody2D body;
	public float radius = 2;
	public float explosionForce = 10;
    public float speed = 10;
    Vector3 direction;

	void Start () 
	{
        body = GetComponent<Rigidbody2D>();

        Destroy(gameObject, 5);
	}
	
	void Update () 
	{
		
	}

    void FixedUpdate()
    {
        body.velocity = direction;
    }

    public void Fire(Vector2 dir)
    {
        direction = dir * speed;

    }

    void OnCollisionEnter2D(Collision2D other)
    {
		Collider2D[] explodedStuff = Physics2D.OverlapCircleAll (
			                             transform.position,
			                             radius);

		foreach (Collider2D hitObject in explodedStuff) 
		{
			if (hitObject.CompareTag ("Asteroid")) 
			{
				Vector2 direction = hitObject.transform.position - transform.position;
				direction += Vector2.up * 5;
				hitObject.attachedRigidbody.AddForce (direction * explosionForce);
			}
				
		}

		Destroy(gameObject);
    }

//    void OnTriggerEnter2D(Collider2D other)
//    {
//        //TODO: physics the rocks away
//
//        Rigidbody2D otherBody = other.GetComponent<Rigidbody2D>();
//
//        if(otherBody != null)
//        {
//            //Vector3 dir = otherBody.transform.position - transform.position;
//            otherBody.AddForce(speed * dir * body.mass);
//        }
//
//        //Debug.Log(other.gameObject.name);
//        Destroy(gameObject);
//    }
}
