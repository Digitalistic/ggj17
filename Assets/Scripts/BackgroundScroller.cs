﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour 
{
    public float scrollSpeed;
    public Transform[] bgPanels;
    public float minYvalue;
    public float panelHeight;

	void Start () 
	{
		
	}
	
	void Update () 
	{
		for(int i=0;i<bgPanels.Length;i++)
        {
            bgPanels[i].transform.Translate(0, -scrollSpeed * Time.deltaTime, 0);
            
        }
	}

    void LateUpdate()
    {
        for(int i=0;i<bgPanels.Length;i++)
        {
            if (bgPanels[i].position.y < minYvalue)
            {
                Vector3 pos = bgPanels[i].transform.position;

                //there should only ever be 3 scroll panels
                if (i == 2)
                    pos.y = bgPanels[0].transform.position.y + panelHeight;
                else if (i == 1)
                    pos.y = bgPanels[2].transform.position.y + panelHeight;
                else
                    pos.y = bgPanels[1].transform.position.y + panelHeight;

                bgPanels[i].transform.position = pos;
            }
        }
    }
}
