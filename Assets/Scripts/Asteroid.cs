﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour 
{
    Rigidbody2D myBody;
    float maxFallSpeed = -8.0f;
    Vector2 myVelo;

    public Sprite[] asteroidImages;

    public bool isAsteroidActivated = false;

    void Start()
    {
        myBody = GetComponent<Rigidbody2D>();

        int index = Random.Range(0, asteroidImages.Length);
        GetComponent<SpriteRenderer>().sprite = asteroidImages[index];
    }

	void FixedUpdate()
    {
        //Debug.Log(myBody.velocity.y);

        myVelo = myBody.velocity;
        myVelo.y = Mathf.Clamp(myVelo.y, maxFallSpeed, -(maxFallSpeed * 4));

        myBody.velocity = myVelo;
    }
}
