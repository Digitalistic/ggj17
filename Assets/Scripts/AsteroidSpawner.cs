﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour 
{
    public static AsteroidSpawner instance;

    public GameObject asteroidPrefab;

    bool isTimerMoving = false;
    float spawnInterval = 5.0f;

    float spawnTimer;
    float timeSinceStart;
    float timeSinceLastIncrease = 0;
    bool spawnShouldSpeedUp = false;

    Vector3 spawnPos = Vector3.up * 11;

    int numHoles = 0;
    int indexToSkip1 = -1;
    int indexToSkip2 = -1;
    int indexToSkip3 = -1;

    int playSize = 15;

    void Awake() { instance = this; }

	void Start () 
	{
        spawnTimer = spawnInterval;

        //Time.timeScale = 0.3f;
    }
	
	void Update () 
	{
        if (!isTimerMoving) return;

        timeSinceStart += Time.timeSinceLevelLoad;
        timeSinceLastIncrease += Time.deltaTime;

        if(timeSinceLastIncrease >= 20)
        {
            spawnInterval = (spawnInterval - (spawnInterval * 0.1f));
            timeSinceLastIncrease = 0;
        }

        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0)
            SpawnRocks();
        
	}

    void SpawnRocks()
    {
        numHoles = Random.Range(0, 4);

        //8 total spaces per side

        if (numHoles > 2)
        {
            int oldHole3 = indexToSkip3;
            while(indexToSkip3 == oldHole3)
                indexToSkip3 = Random.Range(1, 9);
        }
        if (numHoles > 1)
        {
            int oldHole2 = indexToSkip2;
            while (indexToSkip2 == oldHole2)
                indexToSkip2 = Random.Range(1, 9);
        }
        if (numHoles > 0)
        {
            int oldHole1 = indexToSkip1;
            while (indexToSkip1 == oldHole1)
                indexToSkip1 = Random.Range(1, 9);
        }

        int rockIndex = 1;
        for (int i=1;i<=playSize;i+=2)
        {
            rockIndex++;
            if (rockIndex == indexToSkip1 || rockIndex == indexToSkip2 || rockIndex == indexToSkip3) continue;

            spawnPos.x = i + (0.25f);

            float randRotation = Random.Range(0.0f, 360.0f);
            Quaternion asteroidRotation = Quaternion.Euler(0, 0, randRotation);

            Vector3 newPos = spawnPos;
            newPos.y += Random.Range(-0.5f, 0.5f);
            GameObject obj = (GameObject)Instantiate(asteroidPrefab, newPos, asteroidRotation);

            newPos.x *= -1;
            newPos.y += Random.Range(-0.5f, 0.5f);
            obj = (GameObject)Instantiate(asteroidPrefab, newPos, Quaternion.identity);
        }

        spawnTimer = spawnInterval;
    }

    public void StopSpawning()
    {
        isTimerMoving = false;
    }

    public void RestartSpawnClock()
    {
        isTimerMoving = true;

        spawnInterval = 5;
        spawnTimer = spawnInterval;
        timeSinceLastIncrease = 0;
        timeSinceStart = 0;
    }
}
