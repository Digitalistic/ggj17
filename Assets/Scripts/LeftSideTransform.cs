﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftSideTransform : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Asteroid")
        {
            Asteroid a = coll.GetComponent<Asteroid>();

            //if (!a.isAsteroidActivated) return;

            coll.transform.position = new Vector2(coll.transform.position.x + 16.5f, 11.0f);


            Rigidbody2D rb2d = coll.GetComponent<Rigidbody2D>();
            Vector2 speed = rb2d.velocity;
            speed.y = speed.y * -1;
            rb2d.velocity = speed;

            a.isAsteroidActivated = false;
        }
    }
}
