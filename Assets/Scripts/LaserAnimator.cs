﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserAnimator : MonoBehaviour 
{
    public SpriteRenderer myRenderer;
    public Sprite[] animSprites;

    int frameIndex = 0;

    float timer;
    public float timePerFrame = 0.15f;

    void Start()
    {
        myRenderer = GetComponent<SpriteRenderer>();
        timer = timePerFrame;
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if(timer <= 0)
        {
            ShowNextFrame();
        }
    }

    void ShowNextFrame()
    {
        frameIndex++;
        if(frameIndex > animSprites.Length-1)
            frameIndex = 0;

        myRenderer.sprite = animSprites[frameIndex];

        timer = timePerFrame;
    }
}
