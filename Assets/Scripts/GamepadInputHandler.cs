﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class GamepadInputHandler : MonoBehaviour
{
    public int playerID;
    Player player;

    float oldMoveX;
    float oldAimX;
    float moveX;
    float aimX;
    bool wasFirePressed;
    bool isFirePressed;

    [SerializeField]
    PlayerController playerToHandleInput;

    [SerializeField]
    PlayerAnimator playerToAnimate;

    public bool useKeyboard;

    float deadZoneX = 0.2f;


    void Start()
    {
        if (ReInput.players.playerCount - 1 >= playerID)
        {
            player = ReInput.players.GetPlayer(playerID);
        }
    }

    void Update()
    {
        if (GamplayManager.instance.gameActive)
            ProcessInput();
        else if (GamplayManager.instance.gameEnded)
            ProcessEndgame();
        else
            ProcessMenu();
    }

    void ProcessInput()
    {
        oldMoveX = moveX;
        oldAimX = aimX;
        wasFirePressed = isFirePressed;

    //    if(useKeyboard)
    //    {
    //        if(playerID == 0)
    //        {
    //            moveX = 0;
    //            if (Input.GetKey(KeyCode.D))
    //                moveX += 1;
    //            if (Input.GetKey(KeyCode.A))
    //                moveX -= 1;

    //            aimX = 0;
    //            if (Input.GetKey(KeyCode.E))
    //                aimX += 1;
    //            if (Input.GetKey(KeyCode.Q))
    //                aimX -= 1;

    //            isFirePressed = Input.GetKey(KeyCode.LeftShift);
    //        }
    //        else
    //        {
    //            moveX = 0;
    //            if (Input.GetKey(KeyCode.L))
    //                moveX += 1;
    //            if (Input.GetKey(KeyCode.J))
    //                moveX -= 1;

    //            aimX = 0;
				//if (Input.GetKey(KeyCode.O))
    //                aimX += 1;
				//if (Input.GetKey(KeyCode.U))
    //                aimX -= 1;

    //            isFirePressed = Input.GetKey(KeyCode.RightShift);
    //        }
    //    }

        //else
        {
            if (player != null)
            {

                moveX = player.GetAxis("Move Horizontal");
                aimX = player.GetAxis("Aim Horizontal");
                isFirePressed = player.GetButton("Shoot");

                //Debug.Log("PlayerID: " + playerID + " leftX: " + moveX + " rightX: " + aimX + " trigger: " + isFirePressed);
            }
        }


        if (playerToHandleInput != null)
            playerToHandleInput.HandleInput(moveX, aimX, isFirePressed);

        if (playerToAnimate != null)
        {
            if (Mathf.Abs(oldMoveX) > deadZoneX && Mathf.Abs(moveX) <= deadZoneX) //transition to a stop
            {
                //Debug.Log("one: " + moveX);

                bool mirror = Mathf.Sign(oldMoveX) > 0;
                playerToAnimate.ChangeAnimState(SpriteAnimState.stopping, mirror, true, SpriteAnimState.idle, mirror, false);
            }
            else if (Mathf.Sign(oldMoveX) != Mathf.Sign(moveX)) //change direction
            {
                //Debug.Log("two: " + moveX);

                bool mirror = Mathf.Sign(moveX) > 0;
                playerToAnimate.ChangeAnimState(SpriteAnimState.turning, mirror, true, SpriteAnimState.moving, mirror, false);
            }
            else if (Mathf.Abs(oldMoveX) < deadZoneX && Mathf.Abs(moveX) >= deadZoneX) //speed up
            {
                //Debug.Log("three: " + moveX);

                bool mirror = Mathf.Sign(moveX) > 0;
                playerToAnimate.ChangeAnimState(SpriteAnimState.turning, mirror, true, SpriteAnimState.moving, mirror, false);
            }
        }
    }

    void ProcessMenu()
    {

    }

    void ProcessEndgame()
    {
        bool restartPressed = false;
        bool quitPressed = false;

        //Debug.Log("a;sdfjasdf;aksdfasdfasf");

        if (useKeyboard)
        {
            restartPressed = Input.GetKey(KeyCode.X);
            quitPressed = Input.GetKey(KeyCode.B);
        }
        else if (player != null)
        {
            restartPressed = player.GetButtonDown("Replay");
            quitPressed = player.GetButtonDown("Quit");
        }

        if (restartPressed)
        {
            GamplayManager.instance.RestartGame();
        }

        if (quitPressed)
            Application.Quit();
    }
}
