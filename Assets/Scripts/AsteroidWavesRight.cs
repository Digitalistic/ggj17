﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidWavesRight : MonoBehaviour {
	public float spawntime = 10.0f;
	public GameObject asteroid;

	void Start() 
	{
		Invoke ("SpawnAsteroids", spawntime);
	}

	void LateUpdate()
	{


	}

	void SpawnAsteroids() 
	{
		for (int i = 0; i < 5; i++)
			Instantiate(asteroid, new Vector3(4.00f, 5.5f, 0), Quaternion.identity);
		Invoke ("SpawnAsteroids", spawntime);
	}

}