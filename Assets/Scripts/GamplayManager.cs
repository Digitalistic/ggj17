﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamplayManager : MonoBehaviour 
{
    public static GamplayManager instance;

    public Transform UIparent;
    Image whiteFlash;

    public bool gameActive = false;
    public bool gameInMenu = false;
    public bool gameEnded = false;

    FxPro myBloomer;

    public Transform endgameUI;
    bool p2wins = false;

    public PlayerController p1;
    public PlayerController p2;

    public Transform beam;
    AudioSource controlsMusic;

    public Transform controlsParent;
    public List<Text> controlTexts;

    public AudioSource gameplayAudio;

    bool p2wonPreviousRound = false;
    int p1winCount = 0;
    int p2winCount = 0;
    int p1streak = 0;
    int p2streak = 0;

    public Text p1winCountText;
    public Text p2winCountText;
    public Text p1streakText;
    public Text p2streakText;

    void Awake() { instance = this; }

	void Start () 
	{
        whiteFlash = UIparent.Find("WhiteFlash").GetComponent<Image>();
        myBloomer = Camera.main.GetComponent<FxPro>();

        endgameUI.gameObject.SetActive(false);

        controlsMusic = GetComponent<AudioSource>();

        //Invoke("TriggerIntro", 1.0f);
        Invoke("FadeOutControls", 8.0f);
    }
	
	void Update () 
	{
		
	}

    public void EndGame(bool playerTwoWins)
    {
        if (!gameActive) return;

        gameActive = false;
        gameInMenu = false;
        gameEnded = false;
        p2wins = playerTwoWins;

        if(p2wins)
        {
            p2winCount++;
            p2streak++;
            p1streak = 0;
        }
        else
        {
            p1winCount++;
            p1streak++;
            p2streak = 0;
        }

        p1.isAlive = false;
        p2.isAlive = false;

        //Color white = Color.white;
        //white.a = 1.0f;
        //whiteFlash.CrossFadeColor(white, 0.2f, true, true);
        iTween.ColorTo(whiteFlash.gameObject, iTween.Hash("a", 1.0f, "time", 0.2f, "easetype", iTween.EaseType.easeInQuad));
        iTween.ColorTo(whiteFlash.gameObject, iTween.Hash("a", 1.0f, "time", 0.21f, "delay", 0.2f, "easetype", iTween.EaseType.easeOutQuad));

        iTween.ValueTo(whiteFlash.gameObject, iTween.Hash("from", 0,"to", 1.0, "time", 0.15, "easetype", iTween.EaseType.easeInQuad, "onupdatetarget", this.gameObject, "onupdate", "UpdateWhiteAlpha"));
        iTween.ValueTo(whiteFlash.gameObject, iTween.Hash("from", 1.0, "to", 0, "time", 0.15, "delay", 0.16, "easetype", iTween.EaseType.easeOutQuad, "onupdatetarget", this.gameObject, "onupdate", "UpdateWhiteAlpha"));

        LowerBeam();

        Invoke("KillAsteroids", 0.15f);
        Invoke("ShowEndgame", 5.0f);
    }

    void KillAsteroids()
    {
        Asteroid[] asteroids = FindObjectsOfType<Asteroid>();

        foreach (Asteroid a in asteroids)
        {
            Destroy(a.gameObject);
        }

        AsteroidSpawner.instance.StopSpawning();
    }

    void UpdateWhiteAlpha(float newAlpha)
    {
        Color c = whiteFlash.color;
        c.a = newAlpha;
        whiteFlash.color = c;
    }

    void UpdateBloomAmount(float newAmount)
    {
        myBloomer.BloomParams.BloomThreshold = newAmount;
    }

    void ShowMenuAnim()
    {

    }

    void ShowEndgame()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", 0.65f, "to", 0.8f, "time", 0.5f, "easetype", iTween.EaseType.easeInOutQuad, "onupdatetarget", this.gameObject, "onupdate", "UpdateBloomAmount"));
        endgameUI.gameObject.SetActive(true);

        gameEnded = true;

        p1winCountText.text = p1winCount.ToString() + " total wins";
        p2winCountText.text = p2winCount.ToString() + " total wins";

        if (p2wins)
        {
            endgameUI.Find("Winner").Find("p2").GetComponent<Text>().enabled = true;

            if (p2streak > 1)
            {
                p2streakText.text = p2streak.ToString() + " win streak!";
                p2streakText.enabled = true;
            }
            else
                p2streakText.enabled = false;

            p1streakText.enabled = false;
        }
        else
        {
            endgameUI.Find("Winner").Find("p1").GetComponent<Text>().enabled = true;

            if (p1streak > 1)
            {
                p1streakText.text = p1streak.ToString() + " win streak!";
                p1streakText.enabled = true;
            }
            else
                p1streakText.enabled = false;

            p2streakText.enabled = false;
        }
    }

    public void RestartGame()
    {
        gameEnded = false;
        gameActive = true;

        p1.RevealPlayer();
        p2.RevealPlayer();

        p1.isAlive = true;
        p2.isAlive = true;

        endgameUI.Find("Winner").Find("p2").GetComponent<Text>().enabled = false;
        endgameUI.Find("Winner").Find("p1").GetComponent<Text>().enabled = false;

        endgameUI.gameObject.SetActive(false);

        RaiseBeam();

        AsteroidSpawner.instance.RestartSpawnClock();
        iTween.ValueTo(gameObject, iTween.Hash("from", 0.8f, "to", 0.65f, "time", 0.25f, "easetype", iTween.EaseType.easeInOutQuad, "onupdatetarget", this.gameObject, "onupdate", "UpdateBloomAmount"));
    }

    void LowerBeam()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", 0.0f, "to", -15.0f, "time", 2.0f, "easetype", iTween.EaseType.easeInOutQuad, "onupdatetarget", this.gameObject, "onupdate", "UpdateBeamLocPos"));
    }

    void RaiseBeam()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", -15.0f, "to", 0.0f, "time", 2.0f, "easetype", iTween.EaseType.easeInOutQuad, "onupdatetarget", this.gameObject, "onupdate", "UpdateBeamLocPos"));
    }

    void UpdateBeamLocPos(float newPos)
    {
        Vector3 pos = beam.localPosition;
        pos.y = newPos;
        beam.localPosition = pos;
    }

    void UpdateControlsVolume(float newValue)
    {
        controlsMusic.volume = newValue;

        Color c;
        foreach (Text t in controlTexts)
        {
            c = t.color;
            c.a = newValue;
            t.color = c;
        }
    }

    void FadeOutControls()
    {
        iTween.ValueTo(gameObject, iTween.Hash("from", 1.0f, "to", 0.0f, "time", 2.0f, "easetype", iTween.EaseType.easeInOutQuad, "onupdatetarget", this.gameObject, "onupdate", "UpdateControlsVolume"));

        foreach (Text t in controlTexts)
            t.enabled = false;

        Invoke("TriggerIntro", 2.1f);
    }

    void TriggerIntro()
    {
        controlsMusic.enabled = false;
        TitleAnimator.instance.BeginPlayingTitleAnimation();
    }

    public void BeginGameplay()
    {
        AsteroidSpawner.instance.RestartSpawnClock();
        p1.RevealPlayer();
        p2.RevealPlayer();

        gameplayAudio.Play();

        Invoke("KillTitleUI", 0.5f);
    }

    void KillTitleUI()
    {
        RaiseBeam();
        TitleAnimator.instance.HideTitle();
    }
}
