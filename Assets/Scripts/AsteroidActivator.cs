﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidActivator : MonoBehaviour 
{
    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Asteroid")
        {
            Asteroid a = coll.GetComponent<Asteroid>();

            a.isAsteroidActivated = true;
        }
    }
}
