﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleAnimator : MonoBehaviour 
{
    /*<TouchTheSkyrim.mp3> 
     * Su
     * per 
     * Astral 
     * Blitz 
     * Ultra 
     * 6
     * 4 
     * Tour
     * na
     * ment 
     * Edition 
     * Black 
     * Ops 
     * Revelations  */


    //default bloom threshold for gameplay: 0.487

    public static TitleAnimator instance;

    bool isPlaying = false;
    float playTimer = 0;

    public Text[] titleObjects;
    int titleIndex = 0;

    public List<float> spawnTimes = new List<float>();

    void Awake()
    {
        instance = this;

        spawnTimes.Add(2.2f);  //su
        spawnTimes.Add(2.55f);  //per
        spawnTimes.Add(2.85f);  //astral
        spawnTimes.Add(3.27f); //blitz
        spawnTimes.Add(3.7f); //ultra
        spawnTimes.Add(4.1f); //6
        spawnTimes.Add(4.3f); //4

        spawnTimes.Add(4.7f); //tour
        spawnTimes.Add(4.85f); //na
        spawnTimes.Add(5.0f); //ment


        spawnTimes.Add(5.42f); //edition
        spawnTimes.Add(6.19f); //black
        spawnTimes.Add(7.04f); //ops
        spawnTimes.Add(7.45f); //revelations
    }

    void Start () 
	{
        foreach (Text t in titleObjects)
            t.enabled = false;
	}
	
	void Update () 
	{
		if(isPlaying)
        {
            playTimer += Time.deltaTime;

            float nextObject = spawnTimes[titleIndex];

            if (playTimer > nextObject)
            {
                titleObjects[titleIndex].enabled = true;

                if (titleIndex < 13)
                {
                    titleIndex++;
                }
                else
                {
                    isPlaying = false;
                    //GamplayManager.instance.BeginGameplay();

                    Invoke("BeginGameplay", 2.0f);
                }
            }
        }
	}

    public void BeginPlayingTitleAnimation()
    {
        isPlaying = true;
        playTimer = 0;

        GetComponent<AudioSource>().Play();
    }

    public void HideTitle()
    {
        foreach (Text t in titleObjects)
            t.enabled = false;
    }

    void BeginGameplay()
    {
        GamplayManager.instance.BeginGameplay();
    }
}
