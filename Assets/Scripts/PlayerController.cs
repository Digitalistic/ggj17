﻿using UnityEngine;
using System.Collections;


public class PlayerController : MonoBehaviour
{

	public float speed;//Floating point variable to store the player's movement speed.

	private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.
	public Rigidbody2D turret;
	public AudioSource mainCamera;
	public AudioClip bulletBig;
	public AudioClip bulletMedium;
	public AudioClip bulletSmall;


    float moveX;
    float aimX;
    bool isTriggerPressed = false;
    bool wasTriggerPressed;

    public GameObject bulletPrefab;
    public GameObject bulletPrefabMedium;
    public GameObject bulletPrefabLarge;

    float shotCharge = 0;

    SpriteRenderer chargeBlue;
    SpriteRenderer chargeGreen;

    public bool isPlayerOne = false;

    LineRenderer liner;
    Transform lineEnd;

    public bool isAlive = true;

    Vector2 movementVector = Vector2.right;

    public ParticleSystem[] thrusters;

	// Use this for initialization
	void Start()
	{
		//Get and store a reference to the Rigidbody2D component so that we can access it.
		rb2d = GetComponent<Rigidbody2D> ();

        liner = turret.transform.Find("FirePoint").GetComponent<LineRenderer>();
        lineEnd = liner.transform.Find("GameObject").transform;

        chargeBlue = transform.Find("ChargeBlue").GetComponent<SpriteRenderer>();
        chargeGreen = transform.Find("ChargeGreen").GetComponent<SpriteRenderer>();
	}

    public void HandleInput(float leftX, float rightX, bool triggerPressed)
    {
        if (!isAlive) return;

        wasTriggerPressed = isTriggerPressed;

        moveX = leftX;
        aimX = rightX;
        isTriggerPressed = triggerPressed;

        float oldShotCharge = shotCharge;

        if (isTriggerPressed)
        {
            shotCharge += Time.deltaTime;

            if (oldShotCharge < 1 && shotCharge >= 1)
            {
                chargeGreen.enabled = true;
            }
            else if(oldShotCharge < 2 && shotCharge >= 2)
            {
                chargeGreen.enabled = false;
                chargeBlue.enabled = true;
            }
        }

        

        if(isTriggerPressed != wasTriggerPressed)
        {
            if (isTriggerPressed)
            {
                //FireBullet();
            }
            else
            {
                FireBullet();

                shotCharge = 0;
            }
        }
    }

    void Update()
    {
        if(isAlive)
        {
            liner.SetPosition(0, liner.transform.position);
            liner.SetPosition(1, lineEnd.position);
        }
        else
        {
            liner.SetPosition(0, liner.transform.position);
            liner.SetPosition(1, liner.transform.position);
        }
    }

	//FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
	void FixedUpdate()
	{
        if (isAlive)
        {
            movementVector.x = moveX;
            rb2d.velocity = movementVector * speed;
            turret.transform.Rotate(0.0f, 0.0f, -aimX * 2);
        }
        else
        {
            movementVector = Vector2.zero;
            rb2d.velocity = movementVector * speed;
        }

	}
		
    void FireBullet()
	{
        if (!isAlive) return;

		Transform firePoint = turret.transform.Find ("FirePoint");
		GameObject obj;

		if (shotCharge >= 2.0f) {
			obj = (GameObject)Instantiate (bulletPrefabLarge, firePoint.transform.position, bulletPrefab.transform.rotation);
			mainCamera.PlayOneShot (bulletBig);
		} else if (shotCharge >= 1.0f) {
			obj = (GameObject)Instantiate (bulletPrefabMedium, firePoint.transform.position, bulletPrefab.transform.rotation);
			mainCamera.PlayOneShot (bulletMedium);
		} else {
			obj = (GameObject)Instantiate (bulletPrefab, firePoint.transform.position, bulletPrefab.transform.rotation);
			mainCamera.PlayOneShot (bulletSmall);
		}
			Bullet b = obj.GetComponent<Bullet> ();
			b.Fire (firePoint.up);

			shotCharge = 0;

        chargeBlue.enabled = false;
        chargeGreen.enabled = false;

			Physics2D.IgnoreCollision (GetComponent<Collider2D> (), obj.GetComponent<Collider2D> (), true);
		
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        //Debug.Log(other.gameObject.name);

        if(other.collider.tag == "Asteroid")
        {
            KillPlayer();
			mainCamera.Play ();
        }
    }

    void KillPlayer()
    {
        //TODO: Make this cooler
        //Destroy(gameObject);

        if (!isAlive) return;
        if (!GamplayManager.instance.gameActive) return;

        isAlive = false;

        ParticleSystem parts = transform.Find("DeathParticles").GetComponent<ParticleSystem>();
        parts.Play();

        GetComponent<PlayerAnimator>().enabled = false;

        GamplayManager.instance.EndGame(isPlayerOne);


        Invoke("HidePlayer", 4.0f);

        //Destroy(gameObject, 10.0f);
    }

    void HidePlayer()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        sr.enabled = false;

        foreach (ParticleSystem thruster in thrusters)
            thruster.Stop();

        turret.GetComponent<SpriteRenderer>().enabled = false;

        ParticleSystem parts = transform.Find("DeathParticles").GetComponent<ParticleSystem>();
        parts.Stop();
    }

    public void RevealPlayer()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        sr.enabled = true;

        turret.GetComponent<SpriteRenderer>().enabled = true;

        foreach (ParticleSystem thruster in thrusters)
            thruster.Play();

        GetComponent<PlayerAnimator>().enabled = true;
    }

    public void EndShotStuff()
    {
        shotCharge = 0;

        chargeGreen.enabled = false;
        chargeBlue.enabled = false;
    }
    
}