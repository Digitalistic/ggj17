﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpriteAnimState
{
    idle,
    turning,
    moving,
    stopping
}

public class PlayerAnimator : MonoBehaviour 
{
    SpriteRenderer myRenderer;

    public Sprite[] idleSprites;
    public Sprite[] turningSprites;
    public Sprite[] movingSprites;
    public Sprite[] stoppingSprites;

    public SpriteAnimState myState = SpriteAnimState.idle;
    public SpriteAnimState nextState = SpriteAnimState.idle;
    public float timePerFrame = 0.15f;
    float timePerFrameFast = 0.05f;

    bool isStateMirrored = false;
    bool nextStateMirrored = false;

    bool isStateFastAnimating = false;
    bool nextStateFastAnimating = false;

    float timer;
    int frameIndex;

	void Start () 
	{
        myRenderer = GetComponent<SpriteRenderer>();

        timer = timePerFrame;
        frameIndex = 0;

        myRenderer.sprite = idleSprites[frameIndex];
	}
	
	void Update () 
	{
        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            ShowNextFrame();
        }
	}

    void ShowNextFrame()
    {
        frameIndex++;

        if (isStateFastAnimating)
            timer = timePerFrameFast;
        else
            timer = timePerFrame;

        Sprite nextFrame = null;
        if (myState == SpriteAnimState.idle)
        {
            if (frameIndex < idleSprites.Length - 1)
                nextFrame = idleSprites[frameIndex];
        }
        else if(myState == SpriteAnimState.turning)
        {
            if (frameIndex < turningSprites.Length - 1)
                nextFrame = turningSprites[frameIndex];
        }
        else if(myState == SpriteAnimState.moving)
        {
            if (frameIndex < movingSprites.Length - 1)
                nextFrame = movingSprites[frameIndex];
        }
        else if(myState == SpriteAnimState.stopping)
        {
            if (frameIndex < stoppingSprites.Length - 1)
                nextFrame = stoppingSprites[frameIndex];
        }

        if (nextFrame == null)
            GotoNextState();
        else
        {
            myRenderer.sprite = nextFrame;
            myRenderer.flipX = isStateMirrored;
        }
    }

    void GotoNextState()
    {
        myState = nextState;
        frameIndex = -1;
        ShowNextFrame();
    }
    
    public void ChangeAnimState(SpriteAnimState newState, bool mirrorSprites, bool animateFast, SpriteAnimState nextState, bool mirrorNextSprites, bool animateNextFast)
    {
        if (newState != myState || mirrorSprites != isStateMirrored)
        {
            myState = newState;
            isStateMirrored = mirrorSprites;

            this.nextState = nextState;
            nextStateMirrored = mirrorNextSprites;

            isStateFastAnimating = animateFast;
            nextStateFastAnimating = animateNextFast;

            //frameIndex = 0;
            frameIndex = 0;
            timer = timePerFrame;
        }
    }
}
